import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { ChatService } from 'src/app/services/chat.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})

export class ProfilePage implements OnInit {

    public task: AngularFireUploadTask;
    public profileImage: string;
    public getCurrentUser: any;

    constructor(
        public router: Router,
        public storage: AngularFireStorage,
        public loadingController: LoadingController,
        private alertController: AlertController,
        public chatService: ChatService
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.getCurrentUser?.photoURL === null || this.getCurrentUser?.photoURL === undefined || this.getCurrentUser?.photoURL === "null") {
            this.profileImage = "../../../assets/icon/dummy_user.png";
        } else {
            this.profileImage = this.getCurrentUser?.photoURL;
        }
    }

    ngOnInit() {
    }

    public backClick() {
        this.router.navigateByUrl('/tabs', { replaceUrl: true });
    }

    public base64ToImage(dataURI: any) {
        const fileDate = dataURI.split(',');
        const byteString = atob(fileDate[1]);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([arrayBuffer], { type: 'image/png' });
        return blob;
    }

    public async takeCamera() {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });

        const image = await Camera.getPhoto({
            quality: 100,
            allowEditing: false,
            resultType: CameraResultType.DataUrl,
            source: CameraSource.Prompt,
            saveToGallery: true
        }).then(async (getPhoto) => {
            await loading.present();
            const file: any = this.base64ToImage(getPhoto.dataUrl);
            const filePath = `profileImages/${this.getCurrentUser?.uid}`;
            const customMetadata = { app: 'Profile Pictures' };
            const fileRef = this.storage.ref(filePath);
            this.task = this.storage.upload(filePath, file, { customMetadata });
            this.task.snapshotChanges().subscribe(() => {
                fileRef.getDownloadURL().subscribe(async (resp) => {
                    const postData = {
                        uid: this.getCurrentUser?.uid,
                        url: resp
                    };
                    this.chatService.updateProfilePic(postData).then((res: any) => {
                        loading.dismiss();
                        if (res.msg && res?.result) {
                            this.profileImage = res.result.photoURL;
                            localStorage.setItem('currentUser', JSON.stringify(res.result));
                        }
                    }, async (er) => {
                        loading.dismiss();
                        const alert = await this.alertController.create({
                            header: 'Upload Failed',
                            message: er.message,
                            buttons: ['ok']
                        });
                        await alert.present();
                    });
                });
            });
        }).catch(async (er) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Upload Failed',
                message: er.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }
}
