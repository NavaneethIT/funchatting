import { Component, OnInit } from '@angular/core';
import { QueryDocumentSnapshot } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { error } from 'protractor';
import { FriendsListModel, UserData } from 'src/app/models/chat-model';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-chatlist',
    templateUrl: './chatlist.page.html',
    styleUrls: ['./chatlist.page.scss'],
})

export class ChatlistPage implements OnInit {

    public chatGroupFriendList: FriendsListModel = [];
    public getCurrentUser: any;
    public loading: HTMLIonLoadingElement;

    constructor(
        private router: Router,
        private chatService: ChatService,
        private loadingController: LoadingController
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    async ngOnInit() {
        this.loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });
        await this.loading.present();

        this.getChatsList();
    }

    public getChatsList() {
        this.chatService.mergeFriendAndGroupList().subscribe((res: any) => {
            this.loading.dismiss();
            this.chatGroupFriendList = [];
            if (res?.length > 0) {
                this.chatGroupFriendList = res;
            }
        }, (error: any) => {
            this.loading.dismiss();
        });
    }

    public getFriendsList() {
        this.chatGroupFriendList = [];
        this.chatService.getSingleUser(this.getCurrentUser.uid).subscribe((res: UserData) => {
            this.loading.dismiss();
            if (res?.friendsList?.length > 0) {
                this.chatGroupFriendList = res.friendsList;
                const getCurrentUser: UserData = JSON.parse(localStorage.getItem('currentUser'));
                getCurrentUser.friendsList = [];
                getCurrentUser.friendsList = res.friendsList;
                localStorage.setItem('currentUser', JSON.stringify(getCurrentUser));
            }
        }, (error) => {
            this.loading.dismiss();
            console.error(error);
        });
    }

    public chatClick(getUser: any) {
        localStorage.setItem('chatUser', JSON.stringify(getUser));
        this.router.navigateByUrl('/chats', { replaceUrl: true });
    }

}
