import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.page.html',
    styleUrls: ['./signin.page.scss'],
})

export class SigninPage implements OnInit {

    public credentialForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private chatService: ChatService,
        private alertController: AlertController,
        private loadingController: LoadingController
    ) { }

    ngOnInit() {
        this.credentialForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    public forgotPasswordClick() {
        this.router.navigateByUrl('forgot', { replaceUrl: true });
    }

    public async signInClick() {
        const loading = await this.loadingController.create({
            message: 'Sign In...',
        });
        await loading.present();

        this.chatService.signin(this.credentialForm.value).then((res: any) => {
            loading.dismiss();
            if (res.msg) {
                localStorage.setItem('currentUser', JSON.stringify(res.result));
                this.router.navigateByUrl('tabs', { replaceUrl: true });
            }
        }, async (error: any) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Sign In Failed',
                message: error.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }

    public signUpClick() {
        this.router.navigateByUrl('signup', { replaceUrl: true });
    }

    get email() {
        return this.credentialForm.get('email');
    }

    get password() {
        return this.credentialForm.get('password');
    }
}
