import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatsPageRoutingModule } from './chats-routing.module';

import { ChatsPage } from './chats.page';
import { ChatService } from 'src/app/services/chat.service';
import { ScrollbarThemeModule } from 'src/app/services/scrollbarDirective';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChatsPageRoutingModule,
        ScrollbarThemeModule
    ],
    declarations: [
        ChatsPage
    ],
    providers: [
        ChatService
    ]
})

export class ChatsPageModule { }
