import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { IonContent, ToastController } from '@ionic/angular';
import { FriendsListModel, UserData } from 'src/app/models/chat-model';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-chats',
    templateUrl: './chats.page.html',
    styleUrls: ['./chats.page.scss']
})

export class ChatsPage implements OnInit {

    @ViewChild('content') content: IonContent;

    public getCurrentUser: UserData;
    public chatUserData: UserData;
    public isFirstMsg: boolean = false;
    public newMessage = '';
    public chatMessage$: Observable<any> = new Observable;
    public chatMessage = [];

    constructor(
        private router: Router,
        private toastController: ToastController,
        private chatService: ChatService
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.chatUserData = JSON.parse(localStorage.getItem('chatUser'));
    }

    ngOnInit() {
        if (!this.chatUserData?.isGroup) {
            const getCurrentChatData = this.getCurrentUser?.friendsList.find((ele: FriendsListModel[0]) => ele.uid === this.chatUserData.uid);
            if (getCurrentChatData && getCurrentChatData?.chatId) {
                this.isFirstMsg = true;
                localStorage.setItem('chatID', getCurrentChatData.chatId);
                this.getNewMessages(getCurrentChatData.chatId);
            }
        } else {
            this.getNewMessages(this.chatUserData.chatId);
        }
    }

    public backClick() {
        localStorage.removeItem('chatUser');
        localStorage.removeItem('chatID');
        this.router.navigateByUrl('/tabs', { replaceUrl: true });
    }

    public sendMessage() {
        if (
            this.newMessage === '' ||
            this.newMessage === null ||
            this.newMessage === undefined
        ) {
            this.presentToast();
        } else {
            this.chatService.addNewMessage(this.newMessage, this.chatUserData).then((res) => {
                this.scrollTo();
                this.newMessage = '';
                if (!this.isFirstMsg) {
                    this.isFirstMsg = true;
                    const getChatId = localStorage.getItem('chatID');
                    this.getNewMessages(getChatId);
                }
            });
        }
    }

    public getNewMessages(getChatId?: any) {
        this.chatMessage = [];
        this.chatService.getMessages(getChatId).subscribe((res: any) => {
            console.log(res);
            this.chatMessage = [];
            this.chatMessage = res;
            this.scrollTo();
        });
    }

    public scrollTo() {
        setTimeout(() => {
            this.content.scrollToBottom();
        }, 300)
    }

    public async presentToast() {
        const toast = await this.toastController.create({
            message: 'Add some message to send',
            duration: 1500,
            color: 'danger',
            animated: true,
            translucent: true,
            position: 'bottom'
        });
        toast.present();
    }
}
