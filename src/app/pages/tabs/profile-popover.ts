import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { AlertController, PopoverController } from "@ionic/angular";
import { ChatService } from "src/app/services/chat.service";

@Component({
    selector: "app-profile-popover",
    template: `
                <ion-list>
                    <ion-item (click)="viewProfileClick()">
                        <ion-label>View Profile</ion-label>
                    </ion-item>

                    <ion-item (click)="signOutClick()">
                        <ion-label>Sign Out</ion-label>
                        <ion-icon name="log-out-outline"></ion-icon>
                    </ion-item>
                </ion-list>
            `
})

export class ProfilePopoverComponent implements OnInit {

    constructor(
        private router: Router,
        private chatService: ChatService,
        private popoverCtrl: PopoverController,
        private alertCtrl: AlertController
    ) { }

    ngOnInit() {
    }

    public viewProfileClick() {
        this.popoverCtrl.dismiss();
        this.router.navigateByUrl('/profile', { replaceUrl: true });
    }

    public async signOutClick() {
        this.popoverCtrl.dismiss();
        const alert = await this.alertCtrl.create({
            header: 'Sign Out',
            message: 'Do you want to Sign Out ?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.chatService.signOut().then((res: any) => {
                            localStorage.removeItem('currentUser');
                            localStorage.removeItem('chatUser');
                            localStorage.removeItem('chatID');
                            this.router.navigateByUrl('/signin', { replaceUrl: true });
                        }, async (error) => {
                            const errorAlert = await this.alertCtrl.create({
                                header: 'Sign Out Failed',
                                message: error.message
                            });
                            await errorAlert.present();
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
}

