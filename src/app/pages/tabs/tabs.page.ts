import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ProfilePopoverComponent } from './profile-popover';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.page.html',
    styleUrls: ['./tabs.page.scss'],
})

export class TabsPage implements OnInit {

    public selectedTab: string = 'chatlist';
    public getCurrentUser: any;
    public profileImage: any;

    constructor(
        private popoverCtrl: PopoverController,
        private router: Router
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.getCurrentUser?.photoURL === null || this.getCurrentUser?.photoURL === undefined || this.getCurrentUser?.photoURL === "null") {
            this.profileImage = "../../../assets/icon/dummy_user.png";
        } else {
            this.profileImage = this.getCurrentUser?.photoURL;
        }
    }

    ngOnInit() {
    }

    public tabClicked(event: any) {
        this.selectedTab = event.tab;
        this.router.navigateByUrl(`tabs/${this.selectedTab}`, { replaceUrl: true });
    }

    async showPopover(event: any ) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            event
        });
        await popover.present();
    }

    public newUserListClick() {
        this.router.navigateByUrl('userlist', { replaceUrl: true });
    }
}
