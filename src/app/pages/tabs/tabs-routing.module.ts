import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'chatlist',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../chatlist/chatlist.module').then(m => m.ChatlistPageModule)
                    }
                ]
            },
            {
                path: 'calls',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../calls/calls.module').then(m => m.CallsPageModule)
                    }
                ]
            }, {
                path: '',
                redirectTo: '/tabs/chatlist',
                pathMatch: 'full'
            }
        ]
    }, {
        path: '',
        redirectTo: '/tabs/chatlist',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class TabsPageRoutingModule { }
