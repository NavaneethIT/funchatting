import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { AlertController, LoadingController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-creategroup',
    templateUrl: './creategroup.page.html',
    styleUrls: ['./creategroup.page.scss'],
})

export class CreategroupPage implements OnInit {

    public usersList: any = [];
    public dupUsersList: any = [];
    public isAllUser: boolean = true;
    public getCurrentUser: any;
    public groupProfileImage: string;
    public newGroupForm: FormGroup;
    public task: AngularFireUploadTask;
    public selecteGroupMembers: any = [];
    public alert: HTMLIonAlertElement;
    loading: HTMLIonLoadingElement;

    constructor(
        private fb: FormBuilder,
        public router: Router,
        public storage: AngularFireStorage,
        public loadingController: LoadingController,
        private alertController: AlertController,
        public chatService: ChatService,
    ) {
        this.usersList = JSON.parse(localStorage.getItem('usersList'));
        this.dupUsersList = JSON.parse(localStorage.getItem('usersList'));
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.groupProfileImage = "../../../assets/icon/group_Icon_Logo.png";

        if (this.usersList.length > 0) {
            this.isAllUser = true;
        } else {
            this.isAllUser = false;
        }
    }

    ngOnInit() {
        this.newGroupForm = this.fb.group({
            groupProfileImage: [''],
            groupName: ['', [Validators.required, Validators.minLength(5)]],
            groupSlogan: ['Hey there! We are using FunChat']
        });

        this.getUsersList();
    }

    public backClick() {
        localStorage.removeItem('usersList');
        this.router.navigateByUrl('/userlist', { replaceUrl: true });
    }

    public base64ToImage(dataURI: any) {
        const fileDate = dataURI.split(',');
        const byteString = atob(fileDate[1]);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([arrayBuffer], { type: 'image/png' });
        return blob;
    }

    public async takeCamera() {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });

        const image = await Camera.getPhoto({
            quality: 100,
            allowEditing: false,
            resultType: CameraResultType.DataUrl,
            source: CameraSource.Prompt,
            saveToGallery: true
        }).then(async (getPhoto) => {
            await loading.present();
            const getGroupId = new Date().getTime();
            const file: any = this.base64ToImage(getPhoto.dataUrl);
            const filePath = `groupProfileImages/${getGroupId}`;
            const customMetadata = { app: 'Group Profile Pictures' };
            const fileRef = this.storage.ref(filePath);
            this.task = this.storage.upload(filePath, file, { customMetadata });
            this.task.snapshotChanges().subscribe(() => {
                fileRef.getDownloadURL().subscribe(async (resp) => {
                    this.groupProfileImage = resp;
                    this.newGroupForm.value.groupProfileImage = resp;
                    loading.dismiss();
                });
            });
        }).catch(async (er) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Upload Failed',
                message: er.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }

    public getUsersList() {
        if (this.usersList?.length > 0) {
            this.usersList.map((res: any) => {
                res.name = res.uid,
                res.type = 'checkbox',
                res.label = res.displayName,
                res.value = res.uid,
                res.checked = false
                res.handler = () => {
                    res.checked = !res.checked;
                    this.dupUsersList.find((ele: any) => {
                        if (ele.uid === res.uid) {
                            ele.checked = res.checked;
                        }
                    });
                }
            });

            this.dupUsersList.map((res: any) => {
                res.name = res.uid,
                    res.type = 'checkbox',
                    res.label = res.displayName,
                    res.value = res.uid,
                    res.checked = false
                res.handler = () => {
                    res.checked = !res.checked;
                }
            });
        }
    }

    public searchUser(event: any) {
        const getVal = event?.target?.value;
        this.usersList = this.dupUsersList;
        if (getVal.trim() == '') {
            this.alert.inputs = [];
            this.alert.inputs = this.usersList;
            return;
        }
        this.usersList = this.usersList.filter((ele: any) => {
            if ((ele.displayName.toLowerCase().indexOf(getVal.toLowerCase())) > -1) {
                return true;
            }
            return false;
        });
        this.alert.inputs = this.usersList;
    }

    public searchClear(event: any) {
        this.usersList = this.dupUsersList;
        this.alert.inputs = [];
        this.alert.inputs = this.dupUsersList;
    }

    public async presentAlertCheckbox() {
        this.alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Select Members',
            message: '<ion-searchbar></ion-searchbar>',
            inputs: this.usersList,
            animated: true,
            backdropDismiss: false,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Ok',
                    handler: () => {
                        this.selecteGroupMembers = [];
                        this.selecteGroupMembers = this.usersList.filter((ele: any) => ele.checked === true);
                    }
                }
            ]
        });

        if (this.usersList?.length > 0) {
            let ele = document.querySelector('ion-searchbar');
            if (ele) {
                ele.addEventListener('input', this.searchUser.bind(this));
                ele.addEventListener('ionClear', this.searchClear.bind(this));
            }
        }
        await this.alert.present();
    }

    public async createGroupClick() {
        const getGroupValue = this.newGroupForm.value;
        let getSelectedMembers = [];
        if (getGroupValue.groupName === '' || getGroupValue.groupName === null || getGroupValue.groupName === undefined) {
            this.createAlert('Please Enter the Group Name');
        } else if (this.selecteGroupMembers?.length <= 0) {
            this.createAlert('Please add group members');
        } else {
            const loading = await this.loadingController.create({
                message: 'Loading...',
                backdropDismiss: false,
                keyboardClose: false
            });
    
            loading.present();

            getSelectedMembers = this.selecteGroupMembers.map((res: any) => {
                const resp = {
                    aboutStatus: res.aboutStatus,
                    displayName: res.displayName,
                    email: res.email,
                    photoURL: res.photoURL,
                    uid: res.uid
                };
                return resp;
            });

            const currentUserRes = {
                aboutStatus: this.getCurrentUser.aboutStatus,
                displayName: this.getCurrentUser.displayName,
                email: this.getCurrentUser.email,
                photoURL: this.getCurrentUser.photoURL,
                uid: this.getCurrentUser.uid,
                ownerID: this.getCurrentUser.uid
            };

            getSelectedMembers.push(currentUserRes);

            const postData = {
                displayName: this.newGroupForm.value.groupName,
                aboutStatus: this.newGroupForm.value.groupSlogan,
                photoURL: this.newGroupForm.value.groupProfileImage ? this.newGroupForm.value.groupProfileImage : null,
                ouid: this.getCurrentUser.uid,
                isGroup: true
            };
            
            this.chatService.createGroup(postData, getSelectedMembers).then((res) => {
                loading.dismiss();
                if (res) {
                    this.newGroupForm = this.fb.group({
                        groupProfileImage: [''],
                        groupName: ['', [Validators.required, Validators.minLength(5)]],
                        groupSlogan: ['Hey there! We are using FunChat']
                    });

                    this.router.navigateByUrl('/tabs', { replaceUrl: true });
                    localStorage.removeItem('usersList');
                }
            }, (error) => {
                loading.dismiss();
                console.log(error);
            });
        }
    }

    public async createAlert(msg: string) {
        const alert = await this.alertController.create({
            header: 'All Fields Required',
            message: msg,
            buttons: ['ok']
        });
        alert.present();
    }

    public get groupName() {
        return this.newGroupForm.get('groupName');
    }

}
