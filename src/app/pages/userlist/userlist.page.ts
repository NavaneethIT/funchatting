import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { UserData } from 'src/app/models/chat-model';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-userlist',
    templateUrl: './userlist.page.html',
    styleUrls: ['./userlist.page.scss'],
})

export class UserlistPage implements OnInit {

    public usersList = [];
    public dupUsersList = [];
    public isAllUser: boolean = true;
    public getCurrentUser: any;
    public isShowSearch: boolean = false;
    public searchText: string = '';

    constructor(
        private router: Router,
        private chatService: ChatService,
        private loadingController: LoadingController
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.getAllUserData();
    }

    public backClick() {
        this.router.navigateByUrl('/tabs', { replaceUrl: true });
    }

    public searchClick() {
        this.isShowSearch = !this.isShowSearch;
    }

    public searchUser(event: any) {
        this.usersList = this.dupUsersList;
        if (event.trim() == '') {
            return;
        }
        this.usersList = this.usersList.filter((ele: any) => {
            if ((ele.displayName.toLowerCase().indexOf(event.toLowerCase())) > -1) {
                return true;
            }
            return false;
        });
    }

    public searchClear(event: any) {
        this.searchText = '';
        this.usersList = this.dupUsersList;
    }

    public async getAllUserData() {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });
        await loading.present();
        this.isAllUser = true;
        this.usersList = [];
        this.dupUsersList = [];
        this.chatService.getAllUser().subscribe((res: any) => {
            loading.dismiss();
            if (res) {
                res = res.filter((ele: any) => ele.payload.doc.id !== this.getCurrentUser.uid).map((el: any) => {
                    const newData = {
                        uid: el.payload.doc.data().uid,
                        displayName: el.payload.doc.data().displayName,
                        aboutStatus: el.payload.doc.data().aboutStatus,
                        email: el.payload.doc.data().email,
                        photoURL: el.payload.doc.data().photoURL
                    };
                    return newData;
                }).sort((a: any, b: any) => {
                    return (a.displayName > b.displayName) ? 1 : ((b.displayName > a.displayName) ? -1 : 0);
                });
                if (res?.length > 0) {
                    this.isAllUser = true;
                    this.usersList = res;
                    this.dupUsersList = res;
                } else {
                    this.isAllUser = false;
                    this.usersList = [];
                    this.dupUsersList = [];
                }
            } else {
                this.isAllUser = false;
                this.usersList = [];
                this.dupUsersList = [];
            }
        });
    }

    public chatClick(getUser: UserData) {
        localStorage.setItem('chatUser', JSON.stringify(getUser));
        this.router.navigateByUrl('/chats', { replaceUrl: true });
    }

    public newGroupChatClick() {
        localStorage.setItem('usersList', JSON.stringify(this.dupUsersList));
        this.router.navigateByUrl('/creategroup', { replaceUrl: true });
    }

}
