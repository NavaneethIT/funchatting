import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { FriendsListModel } from '../models/chat-model';

@Injectable({
    providedIn: 'root'
})

export class ChatService {

    public getCurrentUser: any;

    constructor(
        public afAuth: AngularFireAuth,
        public afs: AngularFirestore
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    public signin(credentials: any) {
        const signInPromise = new Promise((res, rej) => {
            this.afAuth.signInWithEmailAndPassword(credentials.email, credentials.password).then((getRes) => {
                this.getSingleUser(getRes?.user?.uid).subscribe((userResp: any) => {
                    res({
                        msg: true,
                        result: userResp
                    });
                }, (error: any) => {
                    rej(error);
                });
            }).catch((err: any) => {
                rej(err);
            });
        });
        return signInPromise;
    }

    public signUp(credentials: any) {
        const signUpPromise = new Promise((res, rej) => {
            this.afAuth.createUserWithEmailAndPassword(credentials.email, credentials.password).then((getRes: any) => {
                getRes.user.updateProfile({
                    displayName: credentials.userName,
                    photoURL: null
                });
                this.afs.doc(`users/${getRes.user.uid}`).set({
                    uid: getRes.user.uid,
                    email: credentials.email,
                    displayName: credentials.userName,
                    aboutStatus: 'Hey there! I am using FunChat',
                    photoURL: null,
                    friendsList: [],
                    isGroup: false,
                    ouid: getRes.user.uid
                }).then((docRes) => {
                    this.getSingleUser(getRes?.user?.uid).subscribe((userResp: any) => {
                        res({
                            msg: true,
                            result: userResp
                        });
                    }, (error: any) => {
                        rej(error);
                    });
                }).catch((err: any) => {
                    rej(err);
                });
            }).catch((err: any) => {
                rej(err);
            });
        });
        return signUpPromise;
    }

    public signOut() {
        return this.afAuth.signOut();
    }

    public forgetPassword(postData: any) {
        return new Promise((res, rej) => {
            this.afAuth.sendPasswordResetEmail(postData.email).then((resp: any) => {
                res(true);
            }).catch((error: any) => {
                rej(error);
            });
        });
    }

    public updateProfilePic(postData: any) {
        return new Promise((res, rej) => {
            this.afAuth.currentUser.then((userData: any) => {
                userData.updateProfile({
                    photoURL: postData.url
                }).then((gtRes: any) => {
                    this.afs.collection('users').doc(postData.uid).update({ photoURL: postData.url }).then((resp: any) => {
                        this.getSingleUser(postData.uid).subscribe((userResp: any) => {
                            res({
                                msg: true,
                                result: userResp
                            });
                        }, (error: any) => {
                            rej(error);
                        });
                    }).catch((err: any) => {
                        rej(err);
                    });
                })
            });
        });
    }

    public getSingleUser(userId: any) {
        return this.afs.collection('users').doc(userId).snapshotChanges().pipe(map(ele => {
            return ele.payload.data();
        }));
    }

    public getAllUser() {
        return this.afs.collection('users').snapshotChanges();
    }

    public getFriendsList(userId: any) {
        return this.afs.collection('users').doc(userId).snapshotChanges().pipe(map(ele => {
            return ele.payload.data();
        }));
    }

    public updateFriendsList(getCurrentUser: any, getFrdData: any, getChatId: any) {
        const curentUserFriendPostData = {
            uid: getFrdData.uid,
            chatId: getChatId,
            aboutStatus: getFrdData.aboutStatus,
            displayName: getFrdData.displayName,
            email: getFrdData.email,
            photoURL: getFrdData.photoURL
        };

        const chatUserFriendPostData = {
            uid: getCurrentUser.uid,
            chatId: getChatId,
            aboutStatus: getFrdData.aboutStatus,
            displayName: getCurrentUser.displayName,
            email: getFrdData.email,
            photoURL: getFrdData.photoURL
        };

        return new Promise((res, rej) => {
            this.afs.collection('users').doc(getCurrentUser.uid).ref.update({
                friendsList: firebase.default.firestore.FieldValue.arrayUnion(curentUserFriendPostData)
            }).then((resp: any) => {
                this.afs.collection('users').doc(getFrdData.uid).ref.update({
                    friendsList: firebase.default.firestore.FieldValue.arrayUnion(chatUserFriendPostData)
                }).then((respo: any) => {
                    res(respo);
                }).catch((error) => {
                    rej(error);
                });
            }).catch((error: any) => {
                rej(error);
            });
        });
    }

    public addNewMessage(getMsg: any, frdData: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        let getChatId = '';
        const postData = {
            uid: this.getCurrentUser?.uid,
            userName: this.getCurrentUser?.displayName,
            content: getMsg,
            dummyMsg: '',
            receiverUid: frdData?.uid,
            receiverName: frdData?.displayName,
            createdAt: Date.now(),
            isGroup: false,
            isDummy: false
        };

        if (this.getCurrentUser?.friendsList?.length > 0) {
            const getChatUser = this.getCurrentUser?.friendsList?.find((ele: FriendsListModel[0]) => ele.uid === frdData.uid);
            if (getChatUser && getChatUser?.chatId) {
                getChatId = getChatUser?.chatId;
                return this.afs.collection('messages').doc(getChatId).update({
                    messages: firebase.default.firestore.FieldValue.arrayUnion(postData)
                });
            } else {
                getChatId = this.afs.createId();
                const newPostData = {
                    chatId: getChatId,
                    messages: [{
                        uid: this.getCurrentUser?.uid,
                        userName: this.getCurrentUser?.displayName,
                        content: getMsg,
                        dummyMsg: '',
                        receiverUid: frdData?.uid,
                        receiverName: frdData?.displayName,
                        createdAt: Date.now(),
                        isGroup: false,
                        isDummy: false
                    }]
                };
                return new Promise<any>((res, rej) => {
                    this.afs.collection('messages').doc(getChatId).set(newPostData).then((resp: any) => {
                        localStorage.setItem('chatID', getChatId);
                        this.updateFriendsList(this.getCurrentUser, frdData, getChatId).then((upRes: any) => {
                            // this.getSingleUser(postData.uid).subscribe((getUserResp: any) => {
                            this.mergeFriendAndGroupList().subscribe((getUserResp: any) => {
                                console.log(getUserResp);
                                res({
                                    msg: true,
                                    result: getUserResp
                                });
                            }, (error: any) => {
                                rej(error);
                            });

                            // return this.afs.collection('messages', ref => ref.orderBy('createdAt')).doc(getChatId).valueChanges() as Observable<any[]>;
                        }).catch((error: any) => {
                            rej(error);
                        });
                    });
                });
            }
        } else {
            getChatId = this.afs.createId();
            const newPostData = {
                chatId: getChatId,
                messages: [{
                    uid: this.getCurrentUser?.uid,
                    userName: this.getCurrentUser?.displayName,
                    content: getMsg,
                    dummyMsg: '',
                    receiverUid: frdData?.uid,
                    receiverName: frdData?.displayName,
                    createdAt: Date.now(),
                    isGroup: false,
                    isDummy: false
                }]
            };

            return new Promise<any>((res, rej) => {
                this.afs.collection('messages').doc(getChatId).set(newPostData).then((resp: any) => {
                    localStorage.setItem('chatID', getChatId);
                    this.updateFriendsList(this.getCurrentUser, frdData, getChatId).then((upRes: any) => {
                        // this.getSingleUser(postData.uid).subscribe((getUserResp: any) => {
                        this.mergeFriendAndGroupList().subscribe((getUserResp: any) => {
                            console.log(getUserResp);
                            res({
                                msg: true,
                                result: getUserResp
                            });
                        }, (error: any) => {
                            rej(error);
                        });


                        // return this.afs.collection('messages', ref => ref.orderBy('createdAt')).doc(getChatId).valueChanges() as Observable<any[]>;
                    }).catch((error: any) => {
                        rej(error);
                    });
                });
            });
        }
    }

    public getMessages(getChatId: any) {
        let chatId = getChatId ? getChatId : localStorage.getItem('chatID');
        if (chatId) {
            return this.afs.collection('messages', ref => ref.orderBy('createdAt')).doc(chatId).snapshotChanges().pipe(
                map((ele: any) => { return ele.payload.data().messages; })
            );
        }
    }

    public createGroup(postData: any, groupMembers: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        const groupID = this.afs.createId();
        const getChatId = this.afs.createId();
        const db = firebase.default.firestore();
        const groupBatch = db.batch();
        const newBatch = db.batch();
        let length = 0;

        return new Promise(async (res, rej) => {
            if (groupMembers.length > 0) {
                groupMembers.forEach((ele: any, index: any, data: any) => {
                    firebase.default.firestore().collection('groups').doc(ele.uid).get().then((exsRes: any) => {
                        if (!exsRes?.exists) {
                            const docRef = db.collection('groups').doc(ele.uid);
                            postData.groupID = groupID;
                            postData.chatId = getChatId;
                            groupBatch.set(docRef, { groups: [postData] });
                        } else {
                            const updateDocRef = db.collection('groups').doc(ele.uid);
                            postData.groupID = groupID;
                            postData.chatId = getChatId;
                            groupBatch.update(updateDocRef, { groups: firebase.default.firestore.FieldValue.arrayUnion(postData) });
                        }
                        length++;

                        if (length === data.length) {
                            callBatch(this);
                        }
                    }).catch(error => {
                        rej(error);
                    });
                });

                function callBatch(copyThis: any) {
                    groupBatch.commit().then((resp: any) => {
                        copyThis.afs.collection('groupMembers').doc(groupID).set({
                            groupID: groupID,
                            photoURL: postData.photoURL,
                            groupMembers: groupMembers
                        }).then((respo: any) => {
                            let newPostData = {
                                chatId: getChatId,
                                messages: [{
                                    uid: copyThis.getCurrentUser?.uid,
                                    userName: copyThis.getCurrentUser?.displayName,
                                    content: '',
                                    dummyMsg: 'Created Group',
                                    receiverUid: copyThis.getCurrentUser?.uid,
                                    receiverName: copyThis.getCurrentUser?.displayName,
                                    isGroup: true,
                                    isDummy: true,
                                    createdAt: Date.now()
                                }]
                            };
                            copyThis.afs.collection('messages').doc(getChatId).set(newPostData).then((msgRes: any) => {
                                groupMembers.forEach((ele: any) => {
                                    if (ele.uid !== copyThis.getCurrentUser?.uid) {
                                        const docRef = db.collection('messages').doc(getChatId);
                                        const msgPostData = {
                                            uid: copyThis.getCurrentUser?.uid,
                                            userName: copyThis.getCurrentUser?.displayName,
                                            content: '',
                                            dummyMsg: 'Added',
                                            receiverUid: ele.uid,
                                            receiverName: ele.displayName,
                                            createdAt: Date.now(),
                                            isGroup: true,
                                            isDummy: true
                                        };
                                        newBatch.update(docRef, { messages: firebase.default.firestore.FieldValue.arrayUnion(msgPostData) });
                                    }
                                });
                                newBatch.commit().then((updateRes: any) => {
                                    res(true);
                                }).catch((error: any) => {
                                    rej(error);
                                });
                            }).catch((error: any) => {
                                rej(error);
                            });
                        }).catch((error: any) => {
                            rej(error);
                        });
                    }).catch((error: any) => {
                        rej(error);
                    });
                }
            }
        });
    }

    public getGroups() {
        return this.afs.collection('groups').doc(firebase.default.auth().currentUser.uid).snapshotChanges().pipe(map((groupEle: any) => {
            return groupEle.payload.data();
        }));
    }

    public getGroupChatList() {
        let getGroupList = [];
        return this.getGroups().pipe(
            switchMap((groups: any) => {
                if (groups) {
                    let getGroupIDList: any = [];
                    groups?.groups.forEach((element: any) => {
                        getGroupIDList.push(element.groupID);
                    });
                    getGroupList = groups?.groups;
                    if (getGroupList?.length > 0) {
                        return this.afs.collection('groupMembers').ref.where(firebase.default.firestore.FieldPath.documentId(), "in", getGroupIDList).get();
                    } else {
                        return [];
                    }
                } else {
                    getGroupList = [];
                    return of(getGroupList);
                }
            }),
            map((groupMembers: any) => {
                if (groupMembers?.docs?.length > 0) {
                    getGroupList.map((groupEle: any) => {
                        const getSingleGroup = groupMembers?.docs.find((ele: any) => ele?.data()?.groupID === groupEle.groupID);
                        groupEle.groupMembers = getSingleGroup?.data()?.groupMembers;
                        groupEle.photoURL = getSingleGroup?.data()?.photoURL;
                    });
                } else {
                    getGroupList = [];
                }
                return getGroupList;
            })
        );
    }

    public mergeFriendAndGroupList() {
        let getChatFrienGroupList: any = [];
        let userData: any;
        let returnGroupChatData: any = [];
        return this.getSingleUser(firebase.default.auth().currentUser.uid).pipe(
            switchMap((userRes: any) => {
                userData = userRes;
                const getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
                getCurrentUser.friendsList = [];
                getCurrentUser.friendsList = userRes.friendsList;
                localStorage.setItem('currentUser', JSON.stringify(getCurrentUser));
                return this.getGroupChatList().pipe(map((res: any) => {
                    return res;
                }));
            }),
            map((groupRes: any) => {
                if (groupRes?.length > 0) {
                    groupRes.forEach((groupEle: any) => {
                        getChatFrienGroupList.push(groupEle);
                    });
                } else {
                    getChatFrienGroupList = [];
                }
                
                if (userData) {
                    userData?.friendsList.forEach((userEle: any) => {
                        getChatFrienGroupList.push(userEle);
                    });
                } else {
                    getChatFrienGroupList = [];
                }

                return getChatFrienGroupList;
            })
        );
    }
}
