import { NgModule, Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[appScrollbarTheme]'
})

export class ScrollbarThemeDirective {
    constructor(el: ElementRef) {
        const stylesheet = `
            ::-webkit-scrollbar {
                width: 5px;
            }
            ::-webkit-scrollbar-track {
                background: #ffffff;
            }
            ::-webkit-scrollbar-thumb {
                border-radius: 1rem;
                border: 3px solid #9e9e9e;
            }
            ::-webkit-scrollbar-thumb:hover {
            }
        `;
        // background: linear-gradient(var(--ion-color-light-tint), var(--ion-color-light));
        const styleElmt = el.nativeElement.shadowRoot.querySelector('style');

        if (styleElmt) {
            styleElmt.append(stylesheet);
        } else {
            const barStyle = document.createElement('style');
            barStyle.append(stylesheet);
            el.nativeElement.shadowRoot.appendChild(barStyle);
        }

    }
}

@NgModule({
    declarations: [ScrollbarThemeDirective],
    exports: [ScrollbarThemeDirective]
})

export class ScrollbarThemeModule { }
