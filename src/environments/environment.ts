// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyBd0wQXQdj4W97Qsn83r3w-FM32lZQscY0",
        authDomain: "funchat-d6ac8.firebaseapp.com",
        databaseURL: "https://funchat-d6ac8-default-rtdb.firebaseio.com",
        projectId: "funchat-d6ac8",
        storageBucket: "funchat-d6ac8.appspot.com",
        messagingSenderId: "29191080521",
        appId: "1:29191080521:web:70b857213a3dbb18878d98"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
